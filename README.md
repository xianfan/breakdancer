#Introduction
BreakDancer [1] utilizes discordant read pairs to detect structural variations. Given a binary alignment/map (BAM) file, BreakDancer reports all putative SV calls including deletion (DEL), inversion (INV), intra-chromosomal translocations (ITX), and inter-chromosomal translocations (CTX). Originally proposed as two tools (BreakDancerMax and BreakDancerMini) targeting different scales of SVs, BreakDancerMax was refactored into C++ [2] incorporating samtools API for speedup. The current version supports samtools 0.1.19.

#Install

	Download and compile samtools (version 0.1.19) (http://samtools.sourceforge.net/) on your system 
	Modify the Makefile to point to the samtools folder on your system. Type "make" and enter.

#Usage

    Step 1. Create a configuration file using bam2cfg.pl
    
    e.g.,

    myDirectory/bam2cfg.pl -g -h tumor.bam normal.bam > BRC6.cfg

    bam2cfg now only has the perl version.

    Manually view the insert size and flag distribution results in BRC6.cfg to see if there are any data quality issue. Usually std/mean should be < 0.2 or 0.3 at most. The flag 32(x%), represents percent of chimeric insert, this number (x%) should usually be smaller than 3%.

    View png files for the insert size distribution. You should usually see a normal distribution, a bimodal distribution is undesirable and it is not recommended to continue BreakDancerMax step with this situation existing.

    Step 2. Detect SVs
    
    e.g.,

    myDirectory/breakdancer_max -q 10 -d BRC6.fa BRC6.cfg > BRC6.sv

    The -d option dumps CTX supporting read pairs into fastq files (in this case BRC6.fa) by library. To detect CTX only, use -t option.

    For speedup using parallel runs, please use -o followed by the chromosome name and launch one job per chromosome for intra-chromosomal SVs, including DEL, INS, INV and ITX.

#Dependence over other Perl Modules for bam2cfg.pl

  Statistics::Descriptive;
  Math::CDF;

  which are available at CPAN:

  http://search.cpan.org/~colink/Statistics-Descriptive-2.6/Descriptive.pm
  http://search.cpan.org/~callahan/Math-CDF-0.1/CDF.pm

  Poisson;
  This is provided. Please make sure the "use lib" at the beginning of the scripts contains the correct path.

#Reference

    [1] Chen, K. et al. BreakDancer: an algorithm for high-resolution mapping of genomic structural variation. Nature Methods 6, 677-681 (2009).
    [2] Fan, X., Abbott, T. E., Larson, D. & Chen, K. BreakDancer: Identification of genomic structural variation from Paired-End read mapping. Current Protocols in Bioinformatics Unit 15.6 (2014).
------------------------------------

BreakDancer was initially developed at the Genome Institute of Washington University in St. Louis and was further developed at the University of Texas MD Anderson Cancer Center.

Ken Chen
Xian Fan
MD Anderson Cancer Center

April 9th, 2015